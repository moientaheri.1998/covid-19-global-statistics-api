from django.db import models
from django.contrib.postgres.fields import JSONField


# ----------------------------------------------------------------------------------------------------------------------------

# country (کشور) Model
class Country(models.Model): 
    name = models.CharField(verbose_name = 'نام کشور', max_length = 255, db_index = True, unique = True)
    code = models.CharField(verbose_name = 'کد کشور', max_length = 2, db_index = True, unique = True)
    population = models.BigIntegerField(verbose_name = 'جمعیت', null = True)
    death_rate = models.FloatField(verbose_name = 'نرخ مرگ', null = True)
    recovery_rate = models.FloatField(verbose_name = 'میزان برآوردن', null = True)
    updated_at = models.DateTimeField(verbose_name = 'به روز شده در', null = True)

    def __str__(self):
        return "{} - {}".format(self.name, self.code)

    class Meta:
        ordering = ('id',)   
        verbose_name = "کشور"
        verbose_name_plural = "کشور ها"

# statistics (آمار) Model
class Statistics(models.Model): 
    FK_Country = models.ForeignKey(Country, on_delete = models.SET_NULL, verbose_name = 'کشور', related_name = 'country_statistics', null = True)
    deaths = models.BigIntegerField(verbose_name = 'کل فوت شده ها')
    confirmed = models.BigIntegerField(verbose_name = 'کل موارد تایید شده')
    recovered = models.BigIntegerField(verbose_name = 'کل موارد بهبود یافته')
    critical =  models.BigIntegerField(verbose_name = 'کل موارد بحرانی')
    timeline = JSONField(verbose_name = 'کشور', null = True, blank = True)
    updated_at = models.DateTimeField(verbose_name = 'به روز شده در')

    def __str__(self):
        return "{} : {}".format(self.FK_Country, self.updated_at)

    def save_country(self, this_timeline):
        data = {}
        data['timeline'] = []
        data['timeline'].append(this_timeline)

        self.timeline = data
        self.save()

    class Meta:
        ordering = ('id',)   
        verbose_name = "آمار"
        verbose_name_plural = "آمار ها"