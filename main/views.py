from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone
import threading, requests, json
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_202_ACCEPTED,
    HTTP_500_INTERNAL_SERVER_ERROR,
)
from .serializers import *
# get model
from main.models import Statistics, Country

# ----------------------------------------------------------------------------------------------------------------------------

# get all country //req : form request//result: ({message : 'msg' }) OR error
@csrf_exempt
@api_view(["GET"])
@permission_classes([AllowAny])
def get_all_country(request):
    try:
        # get data
        url = "https://corona-api.com/countries"
        r = requests.get(url, allow_redirects = True)
        open('all_country.json', 'wb').write(r.content)
        # read data from json
        result = None
        with open('all_country.json', encoding = 'utf8') as data:
            result = json.load(data)
        # add to database
        for item in result['data']:
            if not Country.objects.filter(name = item['name']).exists():
                Country.objects.create(name = item['name'], code = item['code'], population = item['population'], death_rate = item['latest_data']['calculated']['death_rate'], recovery_rate = item['latest_data']['calculated']['recovery_rate'], updated_at = item['updated_at'])
            else:
                # get this country
                this_country = Country.objects.get(name = item['name'])
                this_country.death_rate = item['latest_data']['calculated']['death_rate']
                this_country.recovery_rate = item['latest_data']['calculated']['recovery_rate']
                this_country.updated_at = item['updated_at']
                this_country.save()
        return JsonResponse({'message' : 'ok'} , status = HTTP_200_OK)
    except Exception as e:
        return JsonResponse({'message' : str(e)}, status = HTTP_500_INTERNAL_SERVER_ERROR)


# get data //req : form request//result: ({message : 'msg' }) OR error
@csrf_exempt
@api_view(["GET"])
@permission_classes([AllowAny])
def get_data(request):
    try:
        for item in Country.objects.all():
            # get data
            url = "http://corona-api.com/countries/" + item.code
            r = requests.get(url, allow_redirects = True)
            if r.status_code == 200:
                open("data/" + item.name + '.json', 'wb').write(r.content)
                # read data from json
                result = None
                with open("data/" + item.name + '.json', encoding = 'utf8') as data:
                    result = json.load(data)
                # add to database
                if not Statistics.objects.filter(FK_Country = item, updated_at = result['data']['updated_at']).exists():
                    Statistics.objects.create(FK_Country = item, deaths = result['data']['latest_data']['deaths'], confirmed = result['data']['latest_data']['confirmed'], recovered = result['data']['latest_data']['recovered'], critical = result['data']['latest_data']['critical'], timeline = result['data']['timeline'], updated_at = result['data']['updated_at'])
                else:
                    # get this statistics
                    this_statistics = Statistics.objects.get(FK_Country = item)
                    this_statistics.deaths = result['data']['latest_data']['deaths']
                    this_statistics.confirmed = result['data']['latest_data']['confirmed']
                    this_statistics.recovered = result['data']['latest_data']['recovered']
                    this_statistics.critical = result['data']['latest_data']['critical']
                    this_statistics.timeline = result['data']['timeline']
                    this_statistics.updated_at = result['data']['updated_at']
                    this_statistics.save()

                print(item.name)
        return JsonResponse({'message' : 'ok'} , status = HTTP_200_OK)
    except Exception as e:
        return JsonResponse({'message' : str(e)}, status = HTTP_500_INTERNAL_SERVER_ERROR)


# work total case ranking //req : form request//result: ({message : 'msg' }) OR error
@csrf_exempt
@api_view(["GET"])
@permission_classes([AllowAny])
def work_total_case_ranking(request):
    try:
        class Data:
            def __init__(self, item, value):
                self.Country = item
                self.Case = value
            def toJSON(self):
                return json.dumps(self, default = lambda o: o.__dict__)
        # set date to list
        result = []
        for item in Country.objects.all():
            if Statistics.objects.filter(FK_Country = item).exists():
                this_object = Statistics.objects.filter(FK_Country = item).latest('updated_at')
                result.append(Data(item.name, (this_object.confirmed)))
        # get value
        def get_value(item):
            return int(item.Case)
        # sort list
        result.sort(reverse = True, key = get_value)
        # set final list
        final_list = []
        for item in result:
            final_list.append(json.loads(item.toJSON()))
        return JsonResponse(final_list, status = HTTP_200_OK, safe = False)
    except Exception as e:
        return JsonResponse({'message' : str(e)}, status = HTTP_500_INTERNAL_SERVER_ERROR)


# work total death ranking //req : form request//result: ({message : 'msg' }) OR error
@csrf_exempt
@api_view(["GET"])
@permission_classes([AllowAny])
def work_total_death_ranking(request):
    try:
        class Data:
            def __init__(self, item, value):
                self.Country = item
                self.Death = value
            def toJSON(self):
                return json.dumps(self, default = lambda o: o.__dict__)
        # set date to list
        result = []
        for item in Country.objects.all():
            if Statistics.objects.filter(FK_Country = item).exists():
                this_object = Statistics.objects.filter(FK_Country = item).latest('updated_at')
                result.append(Data(item.name, this_object.deaths))
        # get value
        def get_value(item):
            return int(item.Death)
        # sort list
        result.sort(reverse = True, key = get_value)
        # set final list
        final_list = []
        for item in result:
            final_list.append(json.loads(item.toJSON()))
        return JsonResponse(final_list, status = HTTP_200_OK, safe = False)
    except Exception as e:
        return JsonResponse({'message' : str(e)}, status = HTTP_500_INTERNAL_SERVER_ERROR)


# country statistics //req : form request//result: ({message : 'msg' }) OR error
@csrf_exempt
@api_view(["POST"])
@permission_classes([AllowAny])
def country_statistics(request):
    try:
        # get data
        country_id = request.POST.get('country')
        # get country
        this_country = Country.objects.get(id = country_id)
        # get statistics
        this_statistics = Statistics.objects.filter(FK_Country = this_country).latest('updated_at')
        return JsonResponse({'Total-Cases' : this_statistics.confirmed, 'Total-Deaths' : this_statistics.deaths, 'Total-Recoverss' : this_statistics.recovered}, status = HTTP_200_OK, safe = False)
    except Exception as e:
        return JsonResponse({'message' : str(e)}, status = HTTP_500_INTERNAL_SERVER_ERROR)


# country statistics by date //req : form request//result: ({message : 'msg' }) OR error
@csrf_exempt
@api_view(["POST"])
@permission_classes([AllowAny])
def country_statistics_bydate(request):
    try:
        # get data
        country_id = request.POST.get('country')
        this_date = request.POST.get('date')
        # get country
        this_country = Country.objects.get(id = country_id)
        # get statistics
        this_statistics = Statistics.objects.get(FK_Country = this_country)
        # search data
        for item in this_statistics.timeline:
            if item['date'] == this_date:
                return JsonResponse({'Total-Cases' : this_statistics.confirmed, 'Total-Deaths' : this_statistics.deaths, 'Total-Recoverss' : this_statistics.recovered}, status = HTTP_200_OK, safe = False)
            else:
                return JsonResponse({'Total-Cases' : None, 'Total-Deaths' : None, 'Total-Recoverss' : None}, status = HTTP_200_OK, safe = False)
    except Exception as e:
        return JsonResponse({'message' : str(e)}, status = HTTP_500_INTERNAL_SERVER_ERROR)


# index page
def index(request):
    # get country
    countries = Country.objects.all().order_by('name')

    context = {
        'Country':countries, 
    }
    return render(request, 'main.html', context)

# soal_one page
def soal_one(request):
    # get data
    url = "http://localhost:8000/api/word-ranking/total-case/"
    r = requests.get(url, allow_redirects = True)
    if r.status_code == 200:
        open('soal-one.json', 'wb').write(r.content)
        # read data from json
        result = None
        with open('soal-one.json', encoding = 'utf8') as data:
            result = json.load(data)

    context = {
        'Result':result
    }
    return render(request, 'soal_1.html', context)


# soal_two page
def soal_two(request):
    # get data
    url = "http://localhost:8000/api/word-ranking/total-death/"
    r = requests.get(url, allow_redirects = True)
    if r.status_code == 200:
        open('soal-two.json', 'wb').write(r.content)
        # read data from json
        result = None
        with open('soal-two.json', encoding = 'utf8') as data:
            result = json.load(data)

    context = {
        'Result':result
    }
    return render(request, 'soal_2.html', context)


# soal_three page
def soal_three(request):
    # get data
    countryid = request.POST.get('country_one')
    url = "http://localhost:8000/api/total-statistics/country"
    params = {'country': countryid}
    r = requests.post(url, data = params)
    if r.status_code == 200:
        open('soal-three.json', 'wb').write(r.content)
        # read data from json
        result = None
        with open('soal-three.json', encoding = 'utf8') as data:
            result = json.load(data)

    context = {
        'Cases':result['Total-Cases'],
        'Deaths':result['Total-Deaths'],
        'Recovers':result['Total-Recoverss'],
    }
    return render(request, 'resilt.html', context)



# soal_four page
def soal_four(request):
    # get data
    countryid = request.POST.get('country')
    thisdate = request.POST.get('date')
    url = "http://localhost:8000/api/total-statistics-bydate/country"
    params = {'country': countryid, 'date' : thisdate}
    r = requests.post(url, data = params)
    if r.status_code == 200:
        open('soal-four.json', 'wb').write(r.content)
        # read data from json
        result = None
        with open('soal-four.json', encoding = 'utf8') as data:
            result = json.load(data)

    context = {
        'Cases':result['Total-Cases'],
        'Deaths':result['Total-Deaths'],
        'Recovers':result['Total-Recoverss'],
    }
    return render(request, 'resilt.html', context)