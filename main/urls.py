from django.urls import path
from django.conf.urls import url
from .import views

app_name = 'main'
urlpatterns = [
    # get all countries Api <----->
    path('api/get/countries/', views.get_all_country, name = 'getallcountries_api'),
    # get file Api <----->
    path('api/get/data/', views.get_data, name = 'getdata_api'),
    # word total case Api <----->
    path('api/word-ranking/total-case/', views.work_total_case_ranking, name = 'wordtotalcase_api'),
    # word total death Api <----->
    path('api/word-ranking/total-death/', views.work_total_death_ranking, name = 'wordtotaldeath_api'),
    # country statistics Api <----->
    path('api/total-statistics/country', views.country_statistics, name = 'countrystatistics_api'),
    # country statistics by date Api <----->
    path('api/total-statistics-bydate/country', views.country_statistics_bydate, name = 'countrystatisticsbydate_api'),

    # index page <----->
    path('', views.index, name = 'indexpage'),
    # soal_1 page <----->
    path('api/soal-one', views.soal_one, name = 'soal1'),
    # soal_2 page <----->
    path('api/soal-two', views.soal_two, name = 'soal2'),
    # soal_3 page <----->
    path('api/soal-three', views.soal_three, name = 'soal3'),
    # soal_4 page <----->
    path('api/soal-four', views.soal_four, name = 'soal4'),
]