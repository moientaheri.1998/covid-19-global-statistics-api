from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from main.models import Statistics

# --------------------------------------------------------------------------------------------------------------------------------------

class StatisticsSerializer(ModelSerializer):
    class Meta:
        model = Statistics
        fields = [
            'total_cases',
            'total_cases',
            'date',
        ]
